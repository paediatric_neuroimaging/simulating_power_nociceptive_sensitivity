The code in this repository allows you to perform simulations to compare the sample size needed to assess an intervention effect with and without taking into account individual noxious evoked baseline sensitivity. Code is written using MATLAB.

This code relates to Study 2 in 'Quantifying individual noxious-evoked
baseline sensitivity to optimise analgesic trials in neonates' by Maria M. Cobo, Caroline Hartley, Deniz Gursul, Foteini Andritsou, Marianne van der Vaart, Gabriela Schmidt Mellado, Luke Baxter, Eugene P. Duff, Miranda Buckle, Ria Evans Fry, Gabrielle Green, Amy Hoskin, Richard Rogers, Eleri Adams, Fiona Moultrie and Rebeccah Slater.

If you use this code, please cite as:

Cobo M M, Hartley C, Gursul D, Andritsou F, van der Vaart M, Schmidt Mellado G, Baxter L, Duff E P, Buckle M, Evans Fry R, Green G, Hoskin A, Rogers R, Adams E, Moultrie F, Slater R. Quantifying individual noxious-evoked baseline sensitivity to optimise analgesic trials in neonates. eLife 2021;10:e65266


The 3 scripts can be used according to the type of comparison you want to make.
'calculate_power_for_given_intervention_effect.m' can be used to calculate the power (with a 5% significance level) at different group sizes for a given intervention effect (like Figure 2B).
'calculate_number_of_subjects_for_power95_with_diff_interaction.m' can be used to calculate the number of subjects per group needed to achieve a power of 95% (5% significance level) for different levels of the intervention effect (like Figure 2C and D).
'calculate_number_of_subjects_for_power95_with_diff_noise.m' can be used to calculate the number of subjects per group needed to achieve a power of 95% (5% significance level) for different levels of noise - the standard deviation of the residuals in the relationship between nociceptive sensitivity and the response to the clinically required painful procedure (like Figure 2E).

Please follow the notes at the start of each code.

For further help specific to the code email Dr Caroline Hartley (caroline.hartley@paediatrics.ox.ac.uk), or for general correspondance related to the paper contact the corresponding author Prof. Rebeccah Slater (rebeccah.slater@paediatrics.ox.ac.uk).
